package main

import (
	"bufio"
	"fmt"
	"os"

	"task_manager/action"
	"task_manager/model"
	"task_manager/scheduler"
)

var (
	scanner = bufio.NewScanner(os.Stdin)
)

func main() {
	// create inmemory
	model.DataMemory = model.NewInMemory()

	// get menu
	menu()
}

func menu() {

	taskAction := action.NewTaskAction(model.DataMemory)

	for {
		fmt.Println("========= Task Manager tools =========")
		fmt.Println(". Press quit to exit program")
		fmt.Println(". Press 0 to watch list task")
		fmt.Println(". Press 1 to add")
		fmt.Println(". Press 2 to start")
		fmt.Println(". Press 4 to remove all")
		fmt.Println(". Press 5 to export file")
		fmt.Println(". Press 6 to import data from file")
		fmt.Println("========= Task Manager tools =========")
		var option string
		if scanner.Scan() {
			option = scanner.Text()
		}

		switch option {

		case "quit":
			return
		case "0":
			fmt.Println(model.DataMemory.Jobs)
		case "1":
			taskAction.AddTask()
		case "2":
			fmt.Println("===============")
			fmt.Println(". Press 3 to stop")
			fmt.Println("===============")

			s := scheduler.NewScheduler()
			s.SetJobs(model.DataMemory)
			stop := make(chan bool)
			pause := make(chan bool)
			s.Start(stop, pause)
			if scanner.Scan() {
				option = scanner.Text()
			}
			if option == "3" {
				stop <- true
			}
		case "4":
			taskAction.RemoveTask()
		case "5":
			fmt.Println("fill path file you want export")
			if scanner.Scan() {
				option = scanner.Text()
			}
			taskAction.Export(option)
		case "6":
			fmt.Println("fill path file you want import: ")
			if scanner.Scan() {
				option = scanner.Text()
			}
			taskAction.Import(option)
		}
	}

}
