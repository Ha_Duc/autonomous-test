package model

import (
	"fmt"
	"time"
)

type InMemory struct {
	Jobs map[string]Job
}

func NewInMemory() *InMemory {
	return &InMemory{
		Jobs: make(map[string]Job),
	}
}

type Task interface {
	DoJob()
}

func (j *Job) DoJob() {
	fmt.Println("Task Name: ", j.Name)
	fmt.Println("Work: ", j.Work)
}

var (
	Loc        = time.Local // Time location, default set by the time.Local (*time.Location)
	DataMemory *InMemory
)

// Job struct keeping information about job
type Job struct {
	Name      string         `json:"name"`
	AtTime    time.Duration  // optional time at which this job runs
	Loc       *time.Location // optional timezone that the atTime is in
	LastRun   time.Time      // datetime of last run
	NextRun   time.Time      // datetime of next run
	TimeUnit  string         `json:"timeUnit"` // define job will scheduled every time on day or specify time.
	Hour      int            `json:"hour"`
	Minutes   int            `json:"minutes"`
	Second    int            `json:"second"`
	Work      string         `json:"work"`
	AccessJob AccessJob      // access data of another job
}

type AccessJob struct {
	JobName      string // name of another job
	DemandAccess bool   // demand access data.
	StatusAccess bool   // status of 2 task do at the same time
}

// NewJob creates a new job with the time interval.
func NewJob(name, work, timeUnit string, hour, min, sec int, access AccessJob) *Job {
	return &Job{
		Name:      name,
		Loc:       Loc,
		LastRun:   time.Unix(0, 0),
		NextRun:   time.Unix(0, 0),
		TimeUnit:  timeUnit,
		Work:      work,
		Hour:      hour,
		Minutes:   min,
		Second:    sec,
		AccessJob: access,
	}
}

// setLocaltime time to midnight
func (j *Job) setLocaltime(t time.Time, hour int) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), hour, 0, 0, 0, j.Loc)
}

// True if the job should be run now
func (j *Job) ShouldRun() bool {
	return time.Now().Unix() >= j.NextRun.Unix()
}

// scheduleNextRun Compute the instant when this job should run next
func (j *Job) ScheduleNextRun() {
	now := time.Now()
	if j.LastRun == time.Unix(0, 0) {
		j.LastRun = now
	}
	switch j.TimeUnit {
	case "time":
		j.NextRun = j.setLocaltime(j.LastRun, 0)
		j.NextRun = j.LastRun.Add(j.AtTime)

	case "day":
		j.NextRun = j.setLocaltime(j.LastRun, 24)
		j.NextRun = j.NextRun.Add(j.AtTime)
	}
}

// set NextRun before check run task
func (j *Job) SetNextRun() {
	j.AtTime = time.Duration(j.Hour)*time.Hour + time.Duration(j.Minutes)*time.Minute + time.Duration(j.Second)*time.Second
	now := time.Now()
	if j.LastRun == time.Unix(0, 0) {
		j.LastRun = now
	}

	switch j.TimeUnit {
	case "time":
		j.NextRun = j.setLocaltime(j.LastRun, 0)

	case "day":
		j.NextRun = j.setLocaltime(j.LastRun, 0)
		j.NextRun = j.NextRun.Add(j.AtTime)
		if j.NextRun.Before(now) {
			j.NextRun = j.setLocaltime(j.LastRun, 24)
			j.NextRun = j.NextRun.Add(j.AtTime)
		}
	}
}

//Run the job and immediately reschedule it
func (j *Job) Run() {
	if j.AccessJob.StatusAccess {
		if job, ok := DataMemory.Jobs[j.AccessJob.JobName]; ok {
			fmt.Printf("===== Task %s access data Task %s ======\n", j.Name, job.Name)

			j.DoJob()
			job.DoJob()
		}
	}
	j.DoJob()
}
