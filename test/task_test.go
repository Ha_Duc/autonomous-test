package test

import (
	"task_manager/action"
	"task_manager/model"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	Name           string
	Args           interface{}
	ExpectedResult interface{}
	ExpectedError  bool
}

const (
	samplePathFile = "data/sample.json"
)

func TestSetNextRun(t *testing.T) {
	dateDay := time.Date(2021, 03, 12, 1, 0, 10, 0, time.Local)
	dateTime := time.Date(2021, 03, 11, 0, 0, 0, 0, time.Local)
	testcases := []TestCase{
		{"Task with timeUnit is day", "day", dateDay, false},
		{"Task with timeUnit is day", "time", dateTime, false},
	}

	for _, testcase := range testcases {
		t.Run(testcase.Name, func(t *testing.T) {
			job := model.NewJob("name", "", testcase.Args.(string), 1, 0, 10, model.AccessJob{})
			job.SetNextRun()
			assert.Equal(t, job.NextRun, testcase.ExpectedResult)
		})
	}
}

func TestScheduleNextRun(t *testing.T) {
	date := time.Date(2021, 03, 12, 1, 0, 10, 0, time.Local)
	testcases := []TestCase{
		{"Task with timeUnit is day", "day", date, false},
	}

	for _, testcase := range testcases {
		t.Run(testcase.Name, func(t *testing.T) {
			job := model.NewJob("name", "", testcase.Args.(string), 1, 0, 10, model.AccessJob{})
			job.SetNextRun()
			job.ScheduleNextRun()
			assert.Equal(t, job.NextRun, testcase.ExpectedResult)
		})
	}
}

func TestImportJob(t *testing.T) {
	testcases := []TestCase{
		{"Test read data not exit file", "", 0, false},
		{"Test read data from file ", samplePathFile, 1, false},
	}

	for _, testcase := range testcases {
		t.Run(testcase.Name, func(t *testing.T) {
			result := model.NewInMemory()
			taskAction := action.NewTaskAction(result)
			taskAction.Import(testcase.Args.(string))

			assert.NotNil(t, result)
			assert.Equal(t, len(result.Jobs), testcase.ExpectedResult)
			assert.Equal(t, testcase.ExpectedError, false)
		})
	}
}

func TestRemoveTask(t *testing.T) {
	testcases := []TestCase{
		{"Test Remove all task", "", nil, false},
	}

	for _, testcase := range testcases {
		t.Run(testcase.Name, func(t *testing.T) {
			result := model.NewInMemory()
			taskAction := action.NewTaskAction(result)

			taskAction.RemoveTask()
			assert.Nil(t, result.Jobs)
			assert.Equal(t, testcase.ExpectedError, false)
		})
	}
}
