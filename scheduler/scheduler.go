package scheduler

import (
	"sort"
	"task_manager/model"
	"time"
)

// MAXJOBNUM max number of jobss
const MAXJOBNUM = 10000

type Scheduler struct {
	jobs [MAXJOBNUM]*model.Job // Array store jobs
	size int                   // Size of jobs which jobs holding.
}

// NewScheduler creates a new scheduler
func NewScheduler() *Scheduler {
	return &Scheduler{
		jobs: [MAXJOBNUM]*model.Job{},
		size: 0,
	}
}

// Start all the pending jobs
// Add seconds ticker
func (s *Scheduler) Start(stopped chan bool, paused chan bool) chan bool {
	ticker := time.NewTicker(1 * time.Second)

	go func() {
		for {
			select {
			case <-ticker.C:
				s.RunPending()

			case <-stopped:
				ticker.Stop()
				return
			}
		}
	}()

	return stopped
}

// RunPending runs all the jobs that are scheduled to run.
func (s *Scheduler) RunPending() {
	runnableJobs, n := s.getRunnableJobs()

	if n != 0 {
		for k, _ := range runnableJobs {
			job := runnableJobs[k]
			go job.Run()
			job.LastRun = time.Now()
			job.ScheduleNextRun()
		}
	}
}

func (s *Scheduler) Len() int {
	return s.size
}

func (s *Scheduler) Swap(i, j int) {
	s.jobs[i], s.jobs[j] = s.jobs[j], s.jobs[i]
}

func (s *Scheduler) Less(i, j int) bool {
	return s.jobs[j].NextRun.Unix() >= s.jobs[i].NextRun.Unix()
}

func (s *Scheduler) SetJobs(inMemory *model.InMemory) {
	for _, job := range inMemory.Jobs {
		s.jobs[s.size] = &job
		s.size++
	}
}

// Get the current runnable jobs, which shouldRun is True
// allow job access to another job
func (s *Scheduler) getRunnableJobs() (runningJobs map[string]*model.Job, n int) {
	runnableJobs := map[string]*model.Job{}
	n = 0
	sort.Sort(s)
	for i := 0; i < s.size; i++ {
		if s.jobs[i].ShouldRun() {
			job := s.jobs[i]
			runnableJobs[job.Name] = job
			n++
		} else {
			break
		}
	}

	for k, _ := range runnableJobs {
		job := runnableJobs[k]

		if job.AccessJob.DemandAccess {
			if _, ok := runnableJobs[job.AccessJob.JobName]; ok {
				job.AccessJob.StatusAccess = true
			}
		}
	}

	return runnableJobs, n
}
