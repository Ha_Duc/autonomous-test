package action

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"

	"task_manager/model"
)

var (
	scanner = bufio.NewScanner(os.Stdin)
)

type TaskAction struct {
	inMemory *model.InMemory
}

func NewTaskAction(inMemory *model.InMemory) *TaskAction {
	return &TaskAction{
		inMemory: inMemory,
	}
}

func (a *TaskAction) AddTask() {
	fmt.Println("Task name")
	name := pressString()
	for {
		if _, ok := a.inMemory.Jobs[name]; !ok {
			break
		}

		fmt.Println("Task name is exit, fill another name!")
		name = pressString()
	}

	// Time Schedule: hour
	fmt.Println("hour: 00<=hour<=24")
	hour := pressInt()
	for hour < 0 || hour > 24 {
		fmt.Println("hour: 00<=hour<=24, please")
		hour = pressInt()
	}

	// Time Schedule: minutes
	fmt.Println("minutes: 00<=minutes<=60")
	minutes := pressInt()
	for minutes < 0 || minutes > 60 {
		fmt.Println("minutes: 00<=minutes<=60, please")
		minutes = pressInt()
	}

	// Time Schedule: second
	fmt.Println("second: 00<=minutes<=60")
	second := pressInt()
	for second < 0 || second > 60 {
		fmt.Println("second: 00<=minutes<=60, please")
		second = pressInt()
	}

	// Time Schedule: type
	fmt.Println("time unit: time or day")
	timeUnit := pressString()
	for timeUnit != "time" && timeUnit != "day" {
		fmt.Println("time unit: time or day, please")
		timeUnit = pressString()
	}

	// work
	fmt.Println("work: fill any work")
	work := pressString()

	// accessJob
	// JobName
	accessJob := model.AccessJob{}
	if len(a.inMemory.Jobs) >= 2 {
		fmt.Println("Do you want access another job when do at the same time? (y)")
		demandAccess := pressString()
		if demandAccess == "y" {
			fmt.Println("Fill name of another job: ")
			nameAnJob := pressString()
			for {
				if _, ok := a.inMemory.Jobs[nameAnJob]; ok {
					break
				}

				fmt.Println("Task name is not exit, fill another name!")
				nameAnJob = pressString()
			}

			accessJob.DemandAccess = true
			accessJob.JobName = nameAnJob
		}

	}
	// submit
	fmt.Println("Are you want add this task? press (y) to submit.")
	submit := pressString()
	if submit == "y" {
		job := model.NewJob(name, work, timeUnit, hour, minutes, second, accessJob)
		job.SetNextRun()
		a.inMemory.Jobs[job.Name] = *job
		fmt.Println("Submit successful!")
	}
}

func pressString() string {
	var option string
	if scanner.Scan() {
		option = scanner.Text()
	}
	return option
}

func pressInt() int {
	var option string
	if scanner.Scan() {
		option = scanner.Text()
	}

	value, err := strconv.Atoi(option)
	if err != nil {
		return -1
	}

	return value
}

func (a *TaskAction) RemoveTask() {
	a.inMemory.Jobs = nil
}

func (a TaskAction) Import(pathFile string) {
	dataBytes, err := ioutil.ReadFile(pathFile)
	if err != nil {
		fmt.Println("Can not read from file")
		return
	}
	var jobs []model.Job
	err = json.Unmarshal(dataBytes, &jobs)

	if err != nil {
		fmt.Println("Can not unmarshal json")
		return
	}

	for i := 0; i < len(jobs); i++ {
		job := jobs[i]
		job.Loc = model.Loc
		a.inMemory.Jobs[job.Name] = job
	}

	fmt.Println("Import successful")
}

func (a TaskAction) Export(pathFile string) {
	jobs := []model.Job{}
	for _, job := range a.inMemory.Jobs {
		jobs = append(jobs, job)
	}

	bytes, err := json.Marshal(jobs)
	if err != nil {
		fmt.Println("Can not marshal json")
		return
	}

	err = ioutil.WriteFile(pathFile, bytes, 0644)
	if err != nil {
		fmt.Println("Can not write file")
		return
	}

	fmt.Println("Export successful")
}
