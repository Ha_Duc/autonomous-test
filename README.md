# autonomous-test


## How to run?
1. To run process : run command line: go run main.go
2. To run unit test: go to floder testing, then run command line: go test + filename.go

## Explain what i do.
### Job Model
* Struct Job will contain information when we fill that's information in console or import. It'll be stored on RAM (I define InMemory to store Job.). In Struct Job, we have some file neccesery such as: NextRun, AtTime, TimeUnit, LastRun. That's all field I use for create scheduled for Task. Field TimeUnit have two value. 1) time -> it's mean when you choose time, it'll run every time on day plus with (hour, minutes, second) you want. If you choose 2) day, it's mean will run at specific time every day.
* In Scheduler file I define some function for handling process schedule of Job. When we run function Start(), ticker is created and it'll push into channel every second, RunPending will run and check on list Jobs and find jobs need run DoJob() if now >= NextRun. In getRunnableJobs() function I check if that's job have permission to access another job and do work at the same time it'll run DoJob() of another Job.
* In console, I define the menu tool for managing task. We can add task, start all task, stop, remove, export, import. Let's run and try it now :))
* With Unit test, I define some test case for check my logic.

### Result
![](Screen_Shot_2021-03-11_at_09.03.59.png)
